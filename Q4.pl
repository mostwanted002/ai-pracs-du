generate_fib(N,T):-(
    1 < N ->(
    M is N-1, O is N-2,
    generate_fib(M,T1), generate_fib(O,T2),
    T is T1+T2
    );
    1 = N-> T is 1;
    0 = N-> T is 0
).