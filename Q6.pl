power(NUM, POW, ANS):-(
    (1 = NUM; 0 = POW)-> ANS is 1;
    (1 = POW)-> ANS is NUM;
    POW1 is (POW - 1),
    power(NUM, POW1, ANS1),
    ANS is NUM*ANS1
    ).